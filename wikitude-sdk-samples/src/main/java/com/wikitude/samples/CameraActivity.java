package com.wikitude.samples;

import android.Manifest;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.wikitude.samples.util.canvaslibrary.CanvasView;
import com.wikitude.sdksamples.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CameraActivity extends AppCompatActivity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private CanvasView canvasView;
    private RelativeLayout parentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        parentView = findViewById(R.id.parentView);
        canvasView = new CanvasView(this);
        parentView.addView(canvasView);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1
        );

        // Create an instance of Camera
        mCamera = getCameraInstance();

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            Log.d("CameraActivity", "There is no Camera!!");
        }
        return c; // returns null if camera is unavailable
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_delete:
                    canvasView.clearCanvas();
                    break;
                case R.id.navigation_save:
                    Bitmap bitmap = canvasView.getBitmap();
                    Log.d("Bullshit:", "" + bitmap);
                    final File Path = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

                    String fileName = "Canvas-" + System.currentTimeMillis() + ".png";
                    File saveFile = new File(Path, fileName);
                    FileOutputStream FOS = null;
                    try {
                        FOS = new FileOutputStream(saveFile);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, FOS);
                        FOS.flush();
                        FOS.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d("Canvas saved:", Environment.getExternalStorageDirectory().getAbsolutePath() + "/Canvas");

                    break;
            }
            return false;
        }
    };
}
