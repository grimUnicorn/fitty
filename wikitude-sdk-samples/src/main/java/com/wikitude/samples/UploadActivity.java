package com.wikitude.samples;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.UUID;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHost;


import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.content.ByteArrayBody;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;

//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.mime.HttpMultipartMode;
//import org.apache.http.entity.mime.MultipartEntity;
//import org.apache.http.entity.mime.content.ByteArrayBody;
//import org.apache.http.entity.mime.content.StringBody;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.protocol.BasicHttpContext;
//import org.apache.http.protocol.HttpContext;


import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.wikitude.sdksamples.R;

public class UploadActivity extends AppCompatActivity {
    private static final int PICK_IMAGE = 1;
    private ImageView imgView;
    private Button upload;
    private EditText caption;
    private Bitmap bitmap;
    private ProgressDialog dialog;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageupload);

        imgView = (ImageView) findViewById(R.id.ImageView);
        upload = (Button) findViewById(R.id.Upload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.uploadtoolbar);
        setSupportActionBar(toolbar);



        upload.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (bitmap == null) {
                    Toast.makeText(getApplicationContext(),
                            "Please select image", Toast.LENGTH_SHORT).show();
                } else {
                    dialog = ProgressDialog.show(UploadActivity.this, "Uploading",
                            "Please wait...", true);
                    new ImageUploadTask().execute();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.imageupload_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.ic_menu_gallery:
                try {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            PICK_IMAGE);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.exception_message),
                            Toast.LENGTH_LONG).show();
                    Log.e(e.getClass().getName(), e.getMessage(), e);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImageUri = data.getData();

//                    String filePath = null;


                    try {
                        InputStream inputStream = getContentResolver().openInputStream(selectedImageUri);
//                        // OI FILE Manager
//                        String filemanagerstring = selectedImageUri.getPath();
//
//                        // MEDIA GALLERY
//                        String selectedImagePath = selectedImageUri.getPath();
//
//                        if (selectedImagePath != null) {
//                            filePath = selectedImagePath;
//                        } else if (filemanagerstring != null) {
//                            filePath = filemanagerstring;
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Unknown path",
//                                    Toast.LENGTH_LONG).show();
//                            Log.e("Bitmap", "Unknown path");
//                        }
//
//                        if (filePath != null) {
//                            decodeFile(filePath);
//                        } else {
//                            bitmap = null;
//                        }

                        decodeFile(inputStream);


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Internal error",
                                Toast.LENGTH_LONG).show();
                        Log.e(e.getClass().getName(), e.getMessage(), e);
                    }
                }
                break;
            default:
        }
    }

    class ImageUploadTask extends AsyncTask <Void, Void, String>{
        @Override
        protected String doInBackground(Void... unsued) {
            try {

                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpPost httpPost = new HttpPost(
                        getString(R.string.WebServiceURL));
                //+ "/cfc/iphonewebservice.cfc?method=uploadPhoto");


                MultipartEntityBuilder entity = MultipartEntityBuilder.create();
                entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);


                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(CompressFormat.PNG, 100, bos);
                byte[] data = bos.toByteArray();
//                entity.addPart("photoId", new StringBody(getIntent()
//                        .getStringExtra("photoId")));
//                entity.addPart("returnformat", new StringBody("json"));

                String content = getIntent().getStringExtra("CONTENT");
                Log.d("Content", content);
                UUID filename = UUID.fromString(content);
                entity.addPart("uploaded", new ByteArrayBody(data,
                        "myImage.jpg"));

                ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
                postParameters.add(new BasicNameValuePair("file", Base64.encodeToString(data, Base64.DEFAULT)));
                postParameters.add(new BasicNameValuePair("name", filename.toString() + ".png"));





//                entity.addPart("photoCaption", new StringBody(caption.getText()
//                        .toString()));

//                httpPost.setEntity(entity.build());
                httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));


                HttpResponse response = httpClient.execute(httpPost,
                        localContext);
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                response.getEntity().getContent(), "UTF-8"));

                String responseHeader= reader.readLine();
                String w3Header = reader.readLine();
                String sResponse = reader.readLine();
                return sResponse;
            } catch (Exception e) {
                Log.d("Error", e.getMessage());
                if (dialog.isShowing())
                    dialog.dismiss();
                //Toast.makeText(getApplicationContext(),
                //        getString(R.string.exception_message),
                //        Toast.LENGTH_LONG).show();
                Log.e(e.getClass().getName(), e.getMessage(), e);
                return null;
            }

            // (null);
        }

        @Override
        protected void onProgressUpdate(Void... unsued) {

        }

        @Override
        protected void onPostExecute(String sResponse) {
            try {
                if (dialog.isShowing())
                    dialog.dismiss();

                if (sResponse.contains("SUCCESS")){
                    Toast.makeText(getApplicationContext(),
                            "Photo uploaded successfully",
                            Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(getApplicationContext(), "Yup",
                            Toast.LENGTH_LONG).show();
                }

//                if (sResponse != null) {
//                    JSONObject JResponse = new JSONObject(sResponse);
//                    int success = JResponse.getInt("SUCCESS");
//                    String message = JResponse.getString("MESSAGE");
//                    if (success == 0) {
//                        Toast.makeText(getApplicationContext(), message,
//                                Toast.LENGTH_LONG).show();
//                    } else {
//                        Toast.makeText(getApplicationContext(),
//                                "Photo uploaded successfully",
//                                Toast.LENGTH_SHORT).show();
//                        caption.setText("");
//                    }
//                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.exception_message),
                        Toast.LENGTH_LONG).show();
                Log.e(e.getClass().getName(), e.getMessage(), e);
            }
        }
    }
//
//    public String getPath(Uri uri) {
//        String[] projection = { MediaStore.Images.Media.DATA };
//        Cursor cursor = managedQuery(uri, projection, null, null, null);
//        if (cursor != null) {
//            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
//            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
//            int column_index = cursor
//                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } else
//            return null;
//    }

    public void decodeFile(InputStream inputStream) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        //o.inJustDecodeBounds = true;
        bitmap = BitmapFactory.decodeStream(inputStream, null, o );


        // The new size we want to scale to
//        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
//        int width_tmp = o.outWidth, height_tmp = o.outHeight;
//        int scale = 1;
//        while (true) {
//            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
//            break;
//            width_tmp /= 2;
//            height_tmp /= 2;
//            scale *= 2;
//        }

        // Decode with inSampleSize
//        BitmapFactory.Options o2 = new BitmapFactory.Options();
//        o2.inSampleSize = 1;
//        bitmap = BitmapFactory.decodeFile(filePath, o2);

        imgView.setImageBitmap(bitmap);

    }
}