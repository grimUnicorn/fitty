var World = {
    _myPositionable: null,
    _myImageDrawable: null,
    _myURI: "assets/helmet.png",
    _myOldURI: "assets/helmet.png",

    init: function initFn() {
        this.createOverlays();
    },

    createOverlays: function createOverlaysFn() {
         this.myImageResource = new AR.ImageResource(World._myURI);
//
         // timeout (500, -- hol dir url -> imagedrawble ...
         World._myImageDrawable = new AR.ImageDrawable(this.myImageResource, 1, {
                                              onClick : function() {}
                                              });

        World._myPositionable = new AR.Positionable("myPositionable", {
            drawables: {
                cam: World._myImageDrawable
            }
        });

        setInterval(function(){
             var url = document.getElementById('loadingMessage').innerHTML;
             alert(url);
             if(url != "Loading" && url != World._myOldURI){
                World._myURI = url;
                World._myOldURI = url;
                var tempImageResource = new AR.ImageResource(World._myURI);
                World._myImageDrawable.imageResource = tempImageResource;
             }

             alert("super duper");

        }, 5000);

    }
};

//	worldLoaded: function worldLoadedFn() {
//		var cssDivLeft = " style='display: table-cell;vertical-align: middle; text-align: right; width: 50%; padding-right: 15px;'";
//		var cssDivRight = " style='display: table-cell;vertical-align: middle; text-align: left;'";
//		document.getElementById('loadingMessage').innerHTML =
//			"<div" + cssDivLeft + ">Scan Target &#35;1 (surfer) or any QR/Barcode</div>" +
//			"<div" + cssDivRight + "><img src='assets/surfer.png'></img></div>";
//	}
//};

World.init();
